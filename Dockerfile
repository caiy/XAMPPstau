FROM atlas/athanalysis:21.2.96
ADD . /xampp/XAMPPstau
WORKDIR /xampp/build
RUN source ~/release_setup.sh &&  \
    sudo chown -R atlas /xampp && \
    cmake ../XAMPPstau && \
    make -j4
