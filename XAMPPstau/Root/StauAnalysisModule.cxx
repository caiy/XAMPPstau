#include <SUSYTools/SUSYObjDef_xAOD.h>
#include <XAMPPbase/AnalysisUtils.h>
#include <XAMPPbase/ToolHandleSystematics.h>
#include <XAMPPstau/AnalysisUtils.h>
#include <XAMPPstau/StauAnalysisModule.h>

namespace XAMPP {
    StauAnalysisModule::~StauAnalysisModule() {}
    StauAnalysisModule::StauAnalysisModule(const std::string& myname) :
        BaseAnalysisModule(myname),
        m_electrons(nullptr),
        m_muons(nullptr),
        m_taus(nullptr),
        m_jets(nullptr),
        m_met(nullptr),
        m_elec_container(""),
        m_muon_container(""),
        m_tau_container(""),
        m_jet_container(""),
        m_met_container("MetTST") {
        declareProperty("ElectronContainer", m_elec_container);
        declareProperty("MuonContainer", m_muon_container);
        declareProperty("TauContainer", m_tau_container);
        declareProperty("JetContainer", m_jet_container);
        declareProperty("MissingEt", m_met_container);
    }
    StatusCode StauAnalysisModule::retrieveContainers() {
        ATH_CHECK(retrieve(m_electrons, m_elec_container));
        ATH_CHECK(retrieve(m_muons, m_muon_container));
        ATH_CHECK(retrieve(m_taus, m_tau_container));
        ATH_CHECK(retrieve(m_jets, m_jet_container));
        m_met = m_XAMPPInfo->GetVariableStorage<XAMPPmet>(m_met_container);
        if (m_met == nullptr) {
            ATH_MSG_ERROR("Failed to retrieve the MissingET collection " << m_met_container);
            return StatusCode::FAILURE;
        } else
            ATH_MSG_DEBUG("Successfully retrieved the MissingET collection " << m_met_container);
        return StatusCode::SUCCESS;
    }

}  // namespace XAMPP
