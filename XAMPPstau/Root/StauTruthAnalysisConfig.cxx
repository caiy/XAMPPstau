#include <XAMPPbase/AnalysisUtils.h>
#include <XAMPPstau/StauTruthAnalysisConfig.h>

namespace XAMPP {
    StauTruthAnalysisConfig::StauTruthAnalysisConfig(const std::string& Analysis) : TruthAnalysisConfig(Analysis) {}

    StatusCode StauTruthAnalysisConfig::initializeCustomCuts() {
        //########################################################################
        // Start from the baseline and signal tau candidates
        //########################################################################
        Cut* Least1BaselineTauCut = NewSkimmingCut("N_{#tau}^{baseline}>=1", Cut::CutType::CutInt);
        if (!Least1BaselineTauCut->initialize("n_BaseTau", ">=1")) return StatusCode::FAILURE;

        Cut* Exact1BaselineTauCut = NewSkimmingCut("N_{#tau}^{baseline}==1", Cut::CutType::CutInt);
        if (!Exact1BaselineTauCut->initialize("n_BaseTau", "=1")) return StatusCode::FAILURE;

        Cut* Exact1SignalTauCut = NewCutFlowCut("N_{#tau}^{signal}==1", Cut::CutType::CutInt);
        if (!Exact1SignalTauCut->initialize("n_SignalTau", "=1")) return StatusCode::FAILURE;

        //########################################################################
        //                          B-jet veto
        //########################################################################
        Cut* BVeto = NewCutFlowCut("N_{jets}^{b} = 0", Cut::CutType::CutInt);
        if (!BVeto->initialize("n_BJets", "=0")) return StatusCode::FAILURE;

        //########################################################################
        //                  a very low, fundamental  met cut
        //########################################################################
        Cut* MetCut = NewCutFlowCut("E_{T}^{miss} > 15 GeV", Cut::CutType::CutXAMPPmet);
        if (!MetCut->initialize("TruthMET", ">15000.")) return StatusCode::FAILURE;

        //########################################################################
        //                           Muon event
        //########################################################################
        CutFlow MuonEvent("MuonEvent");
        // Select at least 1 baseline muon
        Cut* BaselineMuonCut = NewSkimmingCut("N_{#mu}^{baseline} #geq 1", Cut::CutType::CutInt);
        if (!BaselineMuonCut->initialize("n_BaseMuon", ">=1")) return StatusCode::FAILURE;
        MuonEvent.push_back(BaselineMuonCut);

        // Select 1 signal muon
        Cut* SignalMuonCut = NewSkimmingCut("N_{#mu}^{signal} = 1", Cut::CutType::CutInt);
        if (!SignalMuonCut->initialize("n_SignalMuon", "=1")) return StatusCode::FAILURE;
        MuonEvent.push_back(SignalMuonCut);

        // Veto additional signal leptons (relax the requirement for base objects)
        Cut* BaselineElecVeto = NewSkimmingCut("N_{e}^{signal} = 0", Cut::CutType::CutInt);
        if (!BaselineElecVeto->initialize("n_SignalElec", "=0")) return StatusCode::FAILURE;
        MuonEvent.push_back(BaselineElecVeto);

        // bveto
        MuonEvent.push_back(BVeto);
        // met
        MuonEvent.push_back(MetCut);
        // baseline taus
        MuonEvent.push_back(Least1BaselineTauCut);
        MuonEvent.push_back(Exact1BaselineTauCut);
        ATH_CHECK(AddToCutFlows(MuonEvent));

        //########################################################################
        //              Electron event
        //########################################################################
        CutFlow ElectronEvent("ElectronEvent");

        // Select at least 1 baseline electron
        Cut* BaselineElecCut = NewSkimmingCut("N_{e}^{baseline} #geq 1", Cut::CutType::CutInt);
        if (!BaselineElecCut->initialize("n_BaseElec", ">=1")) return StatusCode::FAILURE;
        ElectronEvent.push_back(BaselineElecCut);

        // Select 1 signal electron
        Cut* SignalElecCut = NewSkimmingCut("N_{e}^{signal} = 1", Cut::CutType::CutInt);
        if (!SignalElecCut->initialize("n_SignalElec", "=1")) return StatusCode::FAILURE;
        ElectronEvent.push_back(SignalElecCut);

        // Veto additional signal leptons (relax the requirement for base objects)
        Cut* BaselineMuonVeto = NewSkimmingCut("N_{#mu}^{signal} = 0", Cut::CutType::CutInt);
        if (!BaselineMuonVeto->initialize("n_SignalMuon", "=0")) return StatusCode::FAILURE;
        ElectronEvent.push_back(BaselineMuonVeto);

        // bveto
        ElectronEvent.push_back(BVeto);
        // met
        ElectronEvent.push_back(MetCut);
        // baseline taus
        ElectronEvent.push_back(Least1BaselineTauCut);
        ElectronEvent.push_back(Exact1BaselineTauCut);

        ATH_CHECK(AddToCutFlows(ElectronEvent));
        //##############################################################
        //                  Fake-tau event
        //##############################################################
        CutFlow FakeTau("Fake#tau");

        FakeTau.push_back(SignalElecCut->combine(SignalMuonCut, Cut::OR));
        FakeTau.push_back(BaselineMuonVeto->combine(BaselineElecVeto, Cut::OR));
        // Tau-veto
        Cut* TauVeto = NewSkimmingCut("N_{#tau}^{signal} = 0", Cut::CutType::CutInt);
        if (!TauVeto->initialize("n_SignalTau", "=0")) return StatusCode::FAILURE;
        FakeTau.push_back(TauVeto);
        // At least one signal jet
        Cut* SignalJet = NewSkimmingCut("N_{#tau}^{fake} >= 1", Cut::CutType::CutInt);
        if (!SignalJet->initialize("n_SignalJets", ">=1")) return StatusCode::FAILURE;
        ATH_CHECK(AddToCutFlows(FakeTau));
        //##############################################################
        //                  Di-tau event
        //##############################################################
        CutFlow DiTau("2#tau");
        DiTau.push_back(BaselineMuonVeto->combine(BaselineElecVeto, Cut::AND));
        DiTau.push_back(Least1BaselineTauCut);
        Cut* DiTauCut = NewSkimmingCut("N_{#tau}^{signal} = 1", Cut::CutType::CutInt);
        if (!DiTauCut->initialize("n_SignalTau", "=2")) return StatusCode::FAILURE;
        DiTau.push_back(DiTauCut);
        ATH_CHECK(AddToCutFlows(DiTau));

        //########################################################################
        //              Z to \ell\ell stream
        //########################################################################
        CutFlow Zll("Z#toll");
        // baseline leptons
        Cut* AtLeast_1_BaseElec = NewSkimmingCut("N_{e}^{baseline} >= 1", Cut::CutType::CutInt);
        if (!AtLeast_1_BaseElec->initialize("n_BaseElec", ">=1")) return StatusCode::FAILURE;
        AtLeast_1_BaseElec = AtLeast_1_BaseElec->combine(BaselineMuonVeto, Cut::AND);

        Cut* AtLeast_1_BaseMuon = NewSkimmingCut("N_{#mu}^{baseline} >= 1", Cut::CutType::CutInt);
        if (!AtLeast_1_BaseMuon->initialize("n_BaseMuon", ">=1")) return StatusCode::FAILURE;
        AtLeast_1_BaseMuon = AtLeast_1_BaseMuon->combine(BaselineElecVeto, Cut::AND);

        Zll.push_back(AtLeast_1_BaseElec->combine(AtLeast_1_BaseMuon, Cut::OR));

        // signal leptons
        Cut* AtLeast_1_SignalElec = NewCutFlowCut("N_{e}^{signal} >= 1", Cut::CutType::CutInt);
        if (!AtLeast_1_SignalElec->initialize("n_SignalElec", ">=1")) return StatusCode::FAILURE;

        Cut* AtLeast_1_SignalMuon = NewCutFlowCut("N_{#mu}^{signal} >= 1", Cut::CutType::CutInt);
        if (!AtLeast_1_SignalMuon->initialize("n_SignalMuon", ">=1")) return StatusCode::FAILURE;

        Zll.push_back(AtLeast_1_SignalElec->combine(AtLeast_1_SignalMuon, Cut::OR));

        // just 2 baseline leptons
        Cut* Two_BaseElec = NewSkimmingCut("N_{e}^{baseline} == 2", Cut::CutType::CutInt);
        if (!Two_BaseElec->initialize("n_BaseElec", "==2")) return StatusCode::FAILURE;

        Cut* Two_BaseMuon = NewSkimmingCut("N_{#mu}^{baseline} == 2", Cut::CutType::CutInt);
        if (!Two_BaseMuon->initialize("n_BaseMuon", "==2")) return StatusCode::FAILURE;

        Zll.push_back(Two_BaseMuon->combine(Two_BaseElec, Cut::OR));

        // just 2 signal leptons
        Cut* Two_SignalElec = NewCutFlowCut("N_{e}^{signal} == 2", Cut::CutType::CutInt);
        if (!Two_SignalElec->initialize("n_SignalElec", "==2")) return StatusCode::FAILURE;

        Cut* Two_SignalMuon = NewCutFlowCut("N_{#mu}^{signal} == 2", Cut::CutType::CutInt);
        if (!Two_SignalMuon->initialize("n_SignalMuon", "==2")) return StatusCode::FAILURE;

        Zll.push_back(Two_SignalMuon->combine(Two_SignalElec, Cut::OR));

        // OS
        Cut* OsMuoMuo = NewCutFlowCut("#mu^{+}#mu^{-}", Cut::CutType::CutChar);
        if (!OsMuoMuo->initialize("OS_MuoMuo", "==1")) return StatusCode::FAILURE;
        Cut* OsEleEle = NewCutFlowCut("e^{+}e^{-}", Cut::CutType::CutChar);
        if (!OsEleEle->initialize("OS_EleEle", "==1")) return StatusCode::FAILURE;
        Zll.push_back(OsEleEle->combine(OsMuoMuo, Cut::OR));
        ATH_CHECK(AddToCutFlows(Zll));

        return StatusCode::SUCCESS;
    }
}  // namespace XAMPP
