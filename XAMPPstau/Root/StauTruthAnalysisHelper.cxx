#include <SUSYTools/SUSYObjDef_xAOD.h>
#include <XAMPPbase/AnalysisUtils.h>
#include <XAMPPbase/EventInfo.h>
#include <XAMPPbase/ReconstructedParticles.h>
#include <XAMPPbase/SUSYSystematics.h>
#include <XAMPPbase/SUSYTruthSelector.h>
#include <XAMPPbase/ToolHandleSystematics.h>
#include <XAMPPstau/AnalysisUtils.h>
#include <XAMPPstau/StauTruthAnalysisHelper.h>

namespace XAMPP {
    static CharAccessor acc_passOR("passOR");
    StauTruthAnalysisHelper::StauTruthAnalysisHelper(const std::string& myname) :
        SUSYTruthAnalysisHelper(myname),
        m_NumTauCandidate_OR(2),
        m_doTauBJetOR(true),
        m_mergeSamples(false),
        m_smp_OR_removal() {
        // Specify the maximum number of tau candidates for the overlap removal with jets
        declareProperty("TauCandidatesForOR", m_NumTauCandidate_OR);
        m_smp_OR_removal.declarePropertyFor(this, "SampleOverlapRemoval");
        declareProperty("DoSampleMerging", m_mergeSamples);
        // Give the priority over b-jets rather than taus
        declareProperty("DoTauBJetOR", m_doTauBJetOR);
    }
    bool StauTruthAnalysisHelper::AcceptEvent() {
        if (!SUSYTruthAnalysisHelper::AcceptEvent() || (m_mergeSamples && !m_smp_OR_removal->AcceptEvent())) return false;
        return true;
    }

    StauTruthAnalysisHelper::~StauTruthAnalysisHelper() {}
    StatusCode StauTruthAnalysisHelper::RemoveOverlap() {
        ATH_MSG_DEBUG("OverlapRemoval...");
        ATH_CHECK(XAMPP::RemoveOverlap(m_truth_selection->GetTruthPreElectrons(), m_truth_selection->GetTruthPreElectrons(), 0.05));
        ATH_CHECK(XAMPP::RemoveOverlap(m_truth_selection->GetTruthPreJets(), m_truth_selection->GetTruthPreElectrons(), 0.2));

        ATH_CHECK(XAMPP::RemoveOverlap(m_truth_selection->GetTruthPreElectrons(), m_truth_selection->GetTruthPreJets(), 0.4));
        ATH_CHECK(XAMPP::RemoveOverlap(m_truth_selection->GetTruthPreMuons(), m_truth_selection->GetTruthPreJets(), 0.4));

        ATH_CHECK(XAMPP::RemoveOverlap(m_truth_selection->GetTruthPreTaus(), m_truth_selection->GetTruthPreElectrons(), 0.2));
        ATH_CHECK(XAMPP::RemoveOverlap(m_truth_selection->GetTruthPreTaus(), m_truth_selection->GetTruthPreMuons(), 0.2));
        ATH_CHECK(XAMPP::RemoveOverlap(m_truth_selection->GetTruthPreTaus(), m_truth_selection->GetTruthPreTaus(), 0.2));

        if (m_doTauBJetOR) {
            ATH_CHECK(XAMPP::RemoveOverlap(m_truth_selection->GetTruthPreTaus(), m_truth_selection->GetTruthPreJets(),
                                           [this](const xAOD::IParticle*, const xAOD::IParticle* jet) -> float {
                                               if (m_truth_selection->IsBJet(jet)) return 0.4;
                                               return 0.;
                                           }));
        }
        //##############################################
        // Perform the OR with the leading tau candidate
        // passing the signal ID.
        //##############################################
        xAOD::TruthParticleContainer LeadTau(SG::VIEW_ELEMENTS);
        unsigned int NTau = 0;
        for (auto itau : *m_truth_selection->GetTruthPreTaus()) {
            if (NTau >= m_NumTauCandidate_OR) break;
            if (acc_passOR(*itau)) {
                LeadTau.push_back(itau);
                ++NTau;
            }
        }
        ATH_CHECK(XAMPP::RemoveOverlap(m_truth_selection->GetTruthPreJets(), &LeadTau, 0.4));

        return StatusCode::SUCCESS;
    }
    StatusCode StauTruthAnalysisHelper::initializeLightLeptonTree() {
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("muons"));
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("electrons"));

        ParticleStorage* MuonStore = m_XAMPPInfo->GetParticleStorage("muons");
        ParticleStorage* ElectronStore = m_XAMPPInfo->GetParticleStorage("electrons");
        StringVector FloatVars{"charge", "MT", /*"polarizationTheta", "polarizationPhi" */};
        StringVector CharVars{"signal"};
        StringVector IntVars{"truthType", "truthOrigin"};
        ATH_CHECK(MuonStore->SaveVariable<float>(FloatVars));
        ATH_CHECK(ElectronStore->SaveVariable<float>(FloatVars));

        ATH_CHECK(MuonStore->SaveVariable<char>(CharVars));
        ATH_CHECK(ElectronStore->SaveVariable<char>(CharVars));

        ATH_CHECK(MuonStore->SaveVariable<int>(IntVars));
        ATH_CHECK(ElectronStore->SaveVariable<int>(IntVars));

        return StatusCode::SUCCESS;
    }
    StatusCode StauTruthAnalysisHelper::initializeJetTree() {
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("jets", true));
        ParticleStorage* JetStore = m_XAMPPInfo->GetParticleStorage("jets");
        StringVector IntVars{};
        StringVector CharVars{"signal", "bjet"};
        StringVector FloatVars{"MT"};
        ATH_CHECK(JetStore->SaveVariable<int>(IntVars));
        ATH_CHECK(JetStore->SaveVariable<char>(CharVars));
        ATH_CHECK(JetStore->SaveVariable<float>(FloatVars));
        return StatusCode::SUCCESS;
    }
    StatusCode StauTruthAnalysisHelper::initializeDiLeptonTree() {
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("dilepton", true));
        ParticleStorage* DiLepStore = m_XAMPPInfo->GetParticleStorage("dilepton");
        StringVector FloatVars{"charge"};
        StringVector IntVars{"pdgId"};
        StringVector CharVars;
        ATH_CHECK(DiLepStore->SaveVariable<int>(IntVars));
        ATH_CHECK(DiLepStore->SaveVariable<float>(FloatVars));
        ATH_CHECK(DiLepStore->SaveVariable<char>(CharVars));

        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("trueDilepton", true));
        DiLepStore = m_XAMPPInfo->GetParticleStorage("trueDilepton");
        ATH_CHECK(DiLepStore->SaveVariable<int>(IntVars));
        ATH_CHECK(DiLepStore->SaveVariable<float>(FloatVars));
        ATH_CHECK(DiLepStore->SaveVariable<char>(CharVars));

        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("recoilingJets", true));

        return StatusCode::SUCCESS;
    }
    StatusCode StauTruthAnalysisHelper::initializeInitPartTree() {
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("initialParticles", true));
        ParticleStorage* InitStore = m_XAMPPInfo->GetParticleStorage("initialParticles");
        StringVector FloatVars{"charge", "CosThetaStar", "TauDecay", "DistDecay", /*"polarizationTheta", "polarizationPhi" */};
        StringVector IntVars{"pdgId"};
        ATH_CHECK(InitStore->SaveVariable<int>(IntVars));
        ATH_CHECK(InitStore->SaveVariable<float>(FloatVars));

        return StatusCode::SUCCESS;
    }
    StatusCode StauTruthAnalysisHelper::initializeTauTree() {
        ATH_CHECK(m_XAMPPInfo->BookParticleStorage("taus", true));
        ParticleStorage* TauStore = m_XAMPPInfo->GetParticleStorage("taus");
        StringVector FloatVars{"MT", "charge", "eta_orig", "phi_orig", "pt_orig", "m_orig",
                               /*"polarizationTheta", "polarizationPhi" */};
        StringVector IntVars{"truthType", "truthOrigin"};
        StringVector CharVars{"signal", "IsHadronicTau", "passOR"};
        ATH_CHECK(TauStore->SaveVariable<int>(IntVars));
        ATH_CHECK(TauStore->SaveVariable<size_t>("numCharged"));
        ATH_CHECK(TauStore->SaveVariable<size_t>("numNeutral"));

        ATH_CHECK(TauStore->SaveVariable<float>(FloatVars));
        ATH_CHECK(TauStore->SaveVariable<char>(CharVars));

        return StatusCode::SUCCESS;
    }
    StatusCode StauTruthAnalysisHelper::initializeEventVariables() {
        ATH_CHECK(initializeLightLeptonTree());
        ATH_CHECK(initializeDiLeptonTree());
        ATH_CHECK(initializeJetTree());
        ATH_CHECK(initializeTauTree());
        ATH_CHECK(initializeInitPartTree());
        ATH_CHECK(initializeModuleContainers());
        if (m_mergeSamples) {
            if (!m_smp_OR_removal.isUserConfigured()) m_smp_OR_removal.setTypeAndName("XAMPP::StauSampleOverlap/MCSampleORremovalTool");
            ATH_CHECK(m_smp_OR_removal.retrieve());
        }

        // Write out the particle multiplicity in the events
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_BaseElec", false));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_SignalElec"));

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_BaseMuon", false));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_SignalMuon"));

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_BaseTau"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_SignalTau"));

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_BaseJets"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_SignalJets"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<int>("n_BJets"));

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<char>("OS_TauEle"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<char>("OS_TauMuo"));

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<char>("OS_TauTau"));

        ATH_CHECK(m_XAMPPInfo->NewEventVariable<char>("OS_EleEle"));
        ATH_CHECK(m_XAMPPInfo->NewEventVariable<char>("OS_MuoMuo"));

        return StatusCode::SUCCESS;
    }
    StatusCode StauTruthAnalysisHelper::initializeModuleContainers() {
        const StringVector stores{"signal_electrons", "signal_muons",   "signal_taus", "signal_jets",
                                  "fake_leptons",     "signal_leptons", "fake_taus"};
        for (const auto& S : stores) {
            ATH_CHECK(m_XAMPPInfo->BookParticleStorage(S));
            ParticleStorage* ParStore = m_XAMPPInfo->GetParticleStorage(S);
            ParStore->SetSaveTrees(false);
            ParStore->SetSaveHistos(false);
        }
        return StatusCode::SUCCESS;
    }
    StatusCode StauTruthAnalysisHelper::ComputeEventVariables() {
        //###############################################################
        //                  Save the objects to the trees
        //###############################################################
        // muons
        static XAMPP::ParticleStorage* MuonStore = m_XAMPPInfo->GetParticleStorage("muons");
        ATH_CHECK(MuonStore->Fill(m_truth_selection->GetTruthBaselineMuons()));
        // electrons
        static XAMPP::ParticleStorage* ElectronStore = m_XAMPPInfo->GetParticleStorage("electrons");
        ATH_CHECK(ElectronStore->Fill(m_truth_selection->GetTruthBaselineElectrons()));
        // jets
        static XAMPP::ParticleStorage* JetStore = m_XAMPPInfo->GetParticleStorage("jets");
        ATH_CHECK(JetStore->Fill(m_truth_selection->GetTruthBaselineJets()));
        // taus
        static XAMPP::ParticleStorage* TauStore = m_XAMPPInfo->GetParticleStorage("taus");
        ATH_CHECK(TauStore->Fill(m_truth_selection->GetTruthPreTaus()));
        // Initial state particles
        static XAMPP::ParticleStorage* InitialStore = m_XAMPPInfo->GetParticleStorage("initialParticles");
        ATH_CHECK(InitialStore->Fill(m_truth_selection->GetTruthPrimaryParticles()));

        static XAMPP::ParticleStorage* Module_SignalMuonStore = m_XAMPPInfo->GetParticleStorage("signal_muons");
        ATH_CHECK(Module_SignalMuonStore->Fill(m_truth_selection->GetTruthSignalMuons()));
        // electrons
        static XAMPP::ParticleStorage* Module_ElectronStore = m_XAMPPInfo->GetParticleStorage("signal_electrons");
        ATH_CHECK(Module_ElectronStore->Fill(m_truth_selection->GetTruthSignalElectrons()));
        // jets
        static XAMPP::ParticleStorage* Module_JetStore = m_XAMPPInfo->GetParticleStorage("signal_jets");
        ATH_CHECK(Module_JetStore->Fill(m_truth_selection->GetTruthSignalJets()));

        // taus
        static XAMPP::ParticleStorage* Module_SigTauStore = m_XAMPPInfo->GetParticleStorage("signal_taus");
        ATH_CHECK(Module_SigTauStore->Fill(m_truth_selection->GetTruthSignalTaus()));

        // Create bulk lepton container
        xAOD::IParticleContainer* Leptons(nullptr);
        xAOD::IParticleContainer* FakeTaus(nullptr);
        xAOD::IParticleContainer* Fake_Leptons(nullptr);

        ATH_CHECK(ViewElementsContainer("Bulk_SignalLeptons", Leptons));
        ATH_CHECK(ViewElementsContainer("FakeTaus", FakeTaus));
        ATH_CHECK(ViewElementsContainer("Fake_SignalLeptons", Fake_Leptons));
        CopyContainer(m_truth_selection->GetTruthSignalElectrons(), Leptons);
        CopyContainer(m_truth_selection->GetTruthSignalMuons(), Leptons);
        CopyContainer(m_truth_selection->GetTruthSignalTaus(), Leptons);

        CopyContainer(m_truth_selection->GetTruthSignalTaus(), FakeTaus);
        // Create the fake lepton container where the fake tau is coming from the jet collection
        CopyContainer(Leptons, Fake_Leptons);
        unsigned int N_Tau = m_truth_selection->GetTruthSignalTaus()->size();
        for (auto jet : *m_truth_selection->GetTruthSignalJets()) {
            if (N_Tau >= m_NumTauCandidate_OR) break;
            Fake_Leptons->push_back(jet);
            FakeTaus->push_back(jet);
            ++N_Tau;
        }
        // signal_leptons
        static XAMPP::ParticleStorage* Module_LeptonStore = m_XAMPPInfo->GetParticleStorage("signal_leptons");
        ATH_CHECK(Module_LeptonStore->Fill(Leptons));

        // fake_leptons
        static XAMPP::ParticleStorage* Module_FakeLeptonStore = m_XAMPPInfo->GetParticleStorage("fake_leptons");
        ATH_CHECK(Module_FakeLeptonStore->Fill(Fake_Leptons));

        static XAMPP::ParticleStorage* Module_FakeTauStore = m_XAMPPInfo->GetParticleStorage("fake_taus");
        ATH_CHECK(Module_FakeTauStore->Fill(FakeTaus));
        FakeTaus->sort(XAMPP::ptsorter);

        static FloatDecorator dec_CosThetaStar("CosThetaStar");
        static FloatDecorator dec_DecayTime("TauDecay");
        static FloatDecorator dec_DecayLen("DistDecay");

        for (auto InitPart : *m_truth_selection->GetTruthPrimaryParticles()) {
            float Cos(-2), DecayTime(-1.e3), DecayLength(-1.e3);

            if (InitPart->hasDecayVtx()) {
                // We're investigating two body decays
                // Take the cosTheta* between the decaying sparticle and the
                // neutral decay product
                for (size_t c = 0; c < InitPart->nChildren(); ++c) {
                    const xAOD::TruthParticle* child = InitPart->child(c);
                    // If the decaying particle is neutral take the negatively charged particle
                    // W->l nu || stau -> tau ninoone
                    // W->qq  || q->Wq
                    if ((fabs(InitPart->threeCharge()) + fabs(child->threeCharge()) == 3) ||
                        ((fabs(child->threeCharge()) == 2 || child->isNeutral()) && InitPart->isCharged()) ||
                        (InitPart->isNeutral() && child->charge() < 0.)) {
                        TLorentzVector Neut = child->p4();
                        Neut.Boost(-GetRestBoost(InitPart));
                        Cos = TMath::Cos(Neut.Angle(GetRestBoost(InitPart)));
                    }
                }
            }
            if (InitPart->hasProdVtx() && InitPart->hasDecayVtx()) {
                TLorentzVector V = InitPart->prodVtx()->v4() - InitPart->decayVtx()->v4();
                DecayTime = TMath::Abs(V.T());
                DecayLength = TMath::Sqrt(V.X() * V.X() + V.Y() * V.Y() + V.Z() * V.Z());
            }
            dec_CosThetaStar(*InitPart) = Cos;
            dec_DecayTime(*InitPart) = DecayTime;
            dec_DecayLen(*InitPart) = DecayLength;
        }
        // Save the number of particles in the final ntuple
        static XAMPP::Storage<int>* dec_nBaseElec = m_XAMPPInfo->GetVariableStorage<int>("n_BaseElec");
        static XAMPP::Storage<int>* dec_nSignalElec = m_XAMPPInfo->GetVariableStorage<int>("n_SignalElec");

        static XAMPP::Storage<int>* dec_nBaseMuon = m_XAMPPInfo->GetVariableStorage<int>("n_BaseMuon");
        static XAMPP::Storage<int>* dec_nSignalMuon = m_XAMPPInfo->GetVariableStorage<int>("n_SignalMuon");

        static XAMPP::Storage<int>* dec_nBaseTau = m_XAMPPInfo->GetVariableStorage<int>("n_BaseTau");
        static XAMPP::Storage<int>* dec_nSignalTau = m_XAMPPInfo->GetVariableStorage<int>("n_SignalTau");

        static XAMPP::Storage<int>* dec_nBaseJets = m_XAMPPInfo->GetVariableStorage<int>("n_BaseJets");
        static XAMPP::Storage<int>* dec_nSignalJets = m_XAMPPInfo->GetVariableStorage<int>("n_SignalJets");
        static XAMPP::Storage<int>* dec_nBJets = m_XAMPPInfo->GetVariableStorage<int>("n_BJets");

        ATH_CHECK(dec_nBaseElec->Store(m_truth_selection->GetTruthBaselineElectrons()->size()));
        ATH_CHECK(dec_nSignalElec->Store(m_truth_selection->GetTruthSignalElectrons()->size()));

        ATH_CHECK(dec_nBaseMuon->Store(m_truth_selection->GetTruthBaselineMuons()->size()));
        ATH_CHECK(dec_nSignalMuon->Store(m_truth_selection->GetTruthSignalMuons()->size()));

        ATH_CHECK(dec_nBaseTau->Store(m_truth_selection->GetTruthBaselineTaus()->size()));
        ATH_CHECK(dec_nSignalTau->Store(m_truth_selection->GetTruthSignalTaus()->size()));

        ATH_CHECK(dec_nBaseJets->Store(m_truth_selection->GetTruthBaselineJets()->size()));
        ATH_CHECK(dec_nSignalJets->Store(m_truth_selection->GetTruthSignalJets()->size()));
        ATH_CHECK(dec_nBJets->Store(m_truth_selection->GetTruthBJets()->size()));

        // Construct the MT
        static XAMPP::Storage<XAMPPmet>* dec_MET = m_XAMPPInfo->GetVariableStorage<XAMPPmet>("TruthMET");
        CalculateMt(m_truth_selection->GetTruthBaselineElectrons(), dec_MET);
        CalculateMt(m_truth_selection->GetTruthBaselineMuons(), dec_MET);
        CalculateMt(m_truth_selection->GetTruthPreTaus(), dec_MET);
        CalculateMt(m_truth_selection->GetTruthBaselineJets(), dec_MET);

        const xAOD::TruthParticle* LeadSignalTau = nullptr;
        if (dec_nSignalTau->GetValue() > 0) LeadSignalTau = m_truth_selection->GetTruthSignalTaus()->at(0);

        const xAOD::TruthParticle* LeadSignalElec = nullptr;
        if (dec_nSignalElec->GetValue() > 0) LeadSignalElec = m_truth_selection->GetTruthSignalElectrons()->at(0);

        const xAOD::TruthParticle* LeadSignalMuon = nullptr;
        if (dec_nSignalMuon->GetValue() > 0) LeadSignalMuon = m_truth_selection->GetTruthSignalMuons()->at(0);

        const xAOD::TruthParticle* SubLeadSignalElec = nullptr;
        if (dec_nSignalElec->GetValue() > 1) SubLeadSignalElec = m_truth_selection->GetTruthSignalElectrons()->at(1);

        const xAOD::TruthParticle* SubLeadSignalMuon = nullptr;
        if (dec_nSignalMuon->GetValue() > 1) SubLeadSignalMuon = m_truth_selection->GetTruthSignalMuons()->at(1);

        const xAOD::TruthParticle* SubLeadSignalTau = nullptr;
        if (dec_nSignalTau->GetValue() > 1) SubLeadSignalTau = m_truth_selection->GetTruthSignalTaus()->at(1);

        // Find the opposite sign pairs
        static XAMPP::Storage<char>* dec_OSTauEle = m_XAMPPInfo->GetVariableStorage<char>("OS_TauEle");
        static XAMPP::Storage<char>* dec_OSTauMuo = m_XAMPPInfo->GetVariableStorage<char>("OS_TauMuo");
        static XAMPP::Storage<char>* dec_OSEleEle = m_XAMPPInfo->GetVariableStorage<char>("OS_EleEle");
        static XAMPP::Storage<char>* dec_OSMuoMuo = m_XAMPPInfo->GetVariableStorage<char>("OS_MuoMuo");
        static XAMPP::Storage<char>* dec_OSTauTau = m_XAMPPInfo->GetVariableStorage<char>("OS_TauTau");

        bool OSEleTau = (LeadSignalTau != nullptr && LeadSignalElec != nullptr && OppositeSign(LeadSignalElec, LeadSignalTau));
        bool OSMuoTau = (LeadSignalTau != nullptr && LeadSignalMuon != nullptr && OppositeSign(LeadSignalMuon, LeadSignalTau));
        bool OSMuoMuo = (LeadSignalMuon != nullptr && SubLeadSignalMuon != nullptr && OppositeSign(SubLeadSignalMuon, LeadSignalMuon));
        bool OSEleEle = (LeadSignalElec != nullptr && SubLeadSignalElec != nullptr && OppositeSign(SubLeadSignalElec, LeadSignalElec));
        bool OSTauTau = (LeadSignalTau != nullptr && SubLeadSignalTau != nullptr && OppositeSign(SubLeadSignalTau, LeadSignalTau));

        ATH_CHECK(dec_OSTauEle->Store(OSEleTau));
        ATH_CHECK(dec_OSTauMuo->Store(OSMuoTau));
        ATH_CHECK(dec_OSEleEle->Store(OSMuoMuo));
        ATH_CHECK(dec_OSMuoMuo->Store(OSEleEle));
        ATH_CHECK(dec_OSTauTau->Store(OSTauTau));

        // Calculate the invariant momenta of the leptons themselves
        ATH_CHECK(m_ParticleConstructor->CreateSubContainer("DiLepCandidates"));
        ConstructInvariantMomenta(
            Leptons, m_ParticleConstructor,
            [this](const xAOD::IParticle* P) -> bool { return m_truth_selection->GetTruthDecorations()->passSignal(*P); }, 2);
        m_ParticleConstructor->DetachSubContainer();
        // Invariant dilepton Candidates
        static ParticleStorage* DiLepStore = m_XAMPPInfo->GetParticleStorage("dilepton");
        xAOD::ParticleContainer* di_leptons = m_ParticleConstructor->GetSubContainer("DiLepCandidates");
        ATH_CHECK(DiLepStore->Fill(di_leptons));

        // TRUTH3 is such a beautiful format half of the leptonic taus are missing
        // and what about the cool concept of particle links? Noooo never. Okay
        // let's go the other way and take all neutrinos in the event summing them up to
        // one fat missing energy  vector
        TLorentzVector total_nu_p4;
        for (const auto nu : *m_truth_selection->GetTruthNeutrinos()) { total_nu_p4 += nu->p4(); }
        // The invariant momenta above were calculated from the visible tau candidates
        // We need another container having the real tau momenta...
        ATH_CHECK(m_ParticleConstructor->CreateSubContainer("TrueDiLepCandidate"));

        for (const auto dilep : *di_leptons) {
            TLorentzVector V = dilep->p4() + total_nu_p4;
            xAOD::Particle* true_dil = m_ParticleConstructor->CreateEmptyParticle();
            true_dil->setPxPyPzE(V.Px(), V.Py(), V.Pz(), V.E());
            true_dil->setCharge(dilep->charge());
            true_dil->setPdgId(dilep->pdgId());
        }
        static ParticleStorage* TrueDiLepStore = m_XAMPPInfo->GetParticleStorage("trueDilepton");
        ATH_CHECK(TrueDiLepStore->Fill(m_ParticleConstructor->GetSubContainer("TrueDiLepCandidate")));
        // Unfortunately the four momenta of the tau and of the lepton do not provide enough information
        // for the neuronal network to learn the mass properly. A despaired way to solve this problem might to
        // look at the total momentum of the jets recoiling against our di-lepton system....
        ATH_CHECK(m_ParticleConstructor->CreateSubContainer("jetMoments"));
        ConstructInvariantMomenta(m_truth_selection->GetTruthSignalJets(), m_ParticleConstructor,
                                  [](const xAOD::IParticle*) { return true; }, 2);
        m_ParticleConstructor->DetachSubContainer();
        xAOD::IParticleContainer jet_moments(SG::VIEW_ELEMENTS);
        CopyContainer(m_truth_selection->GetTruthSignalJets(), jet_moments);
        CopyContainer(m_ParticleConstructor->GetSubContainer("jetMoments"), jet_moments);

        // Store for each di-lepton candidate the jet which is most likely recoiling against it
        // by finding the object minimizing the quantity ( obj - lepton - tau - MET).Pt()
        xAOD::IParticleContainer* balancing_jets = nullptr;

        ATH_CHECK(ViewElementsContainer("BalancingJets", balancing_jets));
        static ParticleStorage* RecoilStore = m_XAMPPInfo->GetParticleStorage("recoilingJets");
        ATH_CHECK(RecoilStore->Fill(balancing_jets));

        TLorentzVector truth_met;
        truth_met.SetPxPyPzE(dec_MET->GetValue()->mpx(), dec_MET->GetValue()->mpy(), 0, dec_MET->GetValue()->met());
        if (jet_moments.size() > 0) {
            for (const auto lep_candidate : *di_leptons) {
                const TLorentzVector inv_lep = truth_met + lep_candidate->p4();
                jet_moments.sort([&inv_lep](const xAOD::IParticle* a, const xAOD::IParticle* b) {
                    return (a->p4() + inv_lep).Pt() < (b->p4() + inv_lep).Pt();
                });
                balancing_jets->push_back(jet_moments.at(0));
                jet_moments.erase(jet_moments.begin());
                if (jet_moments.empty()) break;
            }
        }
        return StatusCode::SUCCESS;
    }
}  // namespace XAMPP
