#include <SUSYTools/ISUSYObjDef_xAODTool.h>
#include <TrigConfInterfaces/ITrigConfigTool.h>
#include <TrigDecisionTool/TrigDecisionTool.h>
#include <TriggerMatchingTool/IMatchingTool.h>
#include <XAMPPbase/EventInfo.h>
#include <XAMPPbase/SUSYTriggerTool.h>
#include <XAMPPbase/TreeHelpers.h>
#include <XAMPPstau/AnalysisUtils.h>
#include <XAMPPstau/StauTriggerTool.h>
#include <xAODTrigMissingET/TrigMissingETContainer.h>
#include <xAODTrigger/EnergySumRoI.h>

namespace XAMPP {
    StauTriggerTool::StauTriggerTool(const std::string& myname) :
        SUSYTriggerTool(myname),
        m_trigConfTool(""),
        m_trigger_tau_store(nullptr) {
        declareProperty("TrigConfigTool", m_trigConfTool);
    }
    StauTriggerTool::~StauTriggerTool() {}
    StatusCode StauTriggerTool::initialize() {
        if (m_init) { return StatusCode::SUCCESS; }
        ATH_CHECK(SUSYTriggerTool::initialize());

        // book the storage element
        ATH_CHECK(m_XAMPPInfo->BookContainerStorage("trigger_tau"));
        m_trigger_tau_store = m_XAMPPInfo->GetContainerStorage("trigger_tau");
        // save the eta  - phi seperately since the trigger_tau is not a full particle in this theme
        ATH_CHECK(m_trigger_tau_store->SaveFloat("phi"));
        ATH_CHECK(m_trigger_tau_store->SaveFloat("eta"));

        return StatusCode::SUCCESS;
    }
    bool StauTriggerTool::CheckTriggerMatching() {
        // try  to retrieve the TauTrigger container from the event store and create a shallow copy
        xAOD::TauJetContainer* trigger_tau = nullptr;
        // Very important the container must not be reorderd or elements must not be added/removed. Please use the ViewElements container
        // for this
        if (CreateContainerLinks("HLT_xAOD__TauJetContainer_TrigTauRecMerged", trigger_tau) == LinkStatus::Failed) {
            ATH_MSG_ERROR("The trigger tau container could not be created. What's going on here?");
            return false;
        }
        // create an empty aux-elements container to shrink the size
        // of objects to be written or to sort them
        xAOD::TauJetContainer* aux_elements = nullptr;
        ATH_CHECK(ViewElementsContainer("aux_elements", aux_elements));
        xAOD::TauJetContainer* pre_taus = m_tau_selection->GetPreTaus();
        // do fancy stuff which I do not know with the trigger taus and taus
        if (pre_taus->empty()) { ATH_MSG_INFO("where are my taus?"); }
        // write the trigger taus to the ntuple
        if (!m_trigger_tau_store->Fill(trigger_tau).isSuccess()) {
            ATH_MSG_ERROR("Why are my trigger taus not in the ntuples?");
            return false;
        }
        // Inherited from the SUSYTriggerTool
        m_trigDecTool.empty();
        m_trigMatchTool.empty();

        return SUSYTriggerTool::CheckTriggerMatching();
    }

}  // namespace XAMPP
