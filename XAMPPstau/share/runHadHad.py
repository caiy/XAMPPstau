from XAMPPstau.SubmitToGrid import getLogicalDataSets
from ClusterSubmission.Utils import ClearFromDuplicates
import os
#include the common library instead of importing it. So we have full access to the globals
# Python you're a strange guy sometimes. Sometimes? Alsmost the entire day
include("XAMPPstau/StauToolSetup.py")
AssembleIO(1)
SetupStauAnaHelper(InitLepHadChannel=False)

#This list is just for Testing of the power to handle tau triggers
SingleTauTriggers = [
    #	"HLT_tau25_idperf_track",
    #	"HLT_tau25_idperf_tracktwo",
    #	"HLT_tau25_perf_track",
    #	"HLT_tau25_perf_tracktwo",
    #	"HLT_tau25_perf_tracktwo_L1TAU12",
    #	"HLT_tau25_loose1_tracktwo",
    #	"HLT_tau25_tight1_tracktwo",
    #	"HLT_tau25_medium1_tracktwo_L1TAU12",
    #	"HLT_tau160_perf_tracktwo",
    #	"HLT_tau160_idperf_track",
    #	"HLT_tau160_idperf_tracktwo",
    "HLT_tau25_medium1_tracktwo",
    "HLT_tau35_medium1_tracktwo",
    "HLT_tau50_medium1_tracktwo_L1TAU12",
    "HLT_tau80_medium1_tracktwo",
    "HLT_tau80_medium1_tracktwo_L1TAU60",
]
CombinedTauTriggers = [
    ###       not availabe until mc16e and data18, no trigSF so put them here
    "HLT_tau60_medium1_tracktwo",
    "HLT_tau125_medium1_tracktwo",
    "HLT_tau160_medium1_tracktwo",
    "HLT_tau160_medium1_tracktwo_L1TAU100",
    "HLT_tau25_medium1_tracktwoEF",
    "HLT_tau35_medium1_tracktwoEF",
    "HLT_tau60_medium1_tracktwoEF",
    "HLT_tau80_medium1_tracktwoEF_L1TAU60",
    "HLT_tau125_medium1_tracktwoEF",
    "HLT_tau160_medium1_tracktwoEF_L1TAU100",
    "HLT_tau25_mediumRNN_tracktwoMVA",
    "HLT_tau35_mediumRNN_tracktwoMVA",
    "HLT_tau60_mediumRNN_tracktwoMVA",
    "HLT_tau80_mediumRNN_tracktwoMVA",
    "HLT_tau80_mediumRNN_tracktwoMVA_L1TAU60",
    "HLT_tau125_mediumRNN_tracktwoMVA",
    "HLT_tau160_mediumRNN_tracktwoMVA_L1TAU100",
    "HLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12",
    "HLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12",
    "HLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40",
    "HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo",
    "HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM",
    "HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50",
    "HLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50",
    "HLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40",
    "HLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40",
    "HLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50",
    "HLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50",
]
#Lepton Triggers
SingleLeptonTriggers = [
    "HLT_e24_lhmedium_L1EM20VH",
    "HLT_e26_lhtight_nod0_ivarloose",
    "HLT_e60_lhmedium_nod0",
    "HLT_e140_lhloose_nod0",
    "HLT_e60_lhmedium",
    "HLT_e120_lhloose",
    #	"HLT_e300_etcut",
    "HLT_mu26_imedium",
    "HLT_mu26_ivarmedium",
    "HLT_mu50",
    "HLT_mu60_0eta105_msonly",
    "HLT_mu20_iloose_L1MU15",
    "HLT_mu40",
    "HLT_mu24_ivarmedium"
]
LeptonTauTriggers = [
    "HLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo",
    "HLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo",
    "HLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50",
    "HLT_mu14_ivarloose_tau35_medium1_tracktwo",
    "HLT_mu14_ivarloose_tau25_medium1_tracktwo",
    "HLT_mu14_tau25_medium1_tracktwo_xe50",
    ###       not availabe until mc16e and data18
    "HLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF",
    "HLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA",
    "HLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50",
    "HLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50",
    "HLT_mu14_ivarloose_tau35_medium1_tracktwoEF",
    "HLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA",
    "HLT_mu14_tau25_medium1_tracktwoEF_xe50",
    "HLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50"
]
METTriggers = [
    "HLT_xe90_pufit_L1XE50",
    "HLT_xe100_pufit_L1XE50",
    "HLT_xe100_pufit_L1XE55",
    "HLT_xe110_pufit_L1XE50",
    "HLT_xe110_pufit_L1XE55",
    "HLT_xe110_pufit_xe65_L1XE50",
    "HLT_xe110_pufit_xe70_L1XE50",
    "HLT_xe120_pufit_L1XE50",
    "HLT_xe90_mht_L1XE50",
    "HLT_xe110_mht_L1XE50",
    "HLT_xe70_mht",
]
TauTriggers = CombinedTauTriggers + SingleTauTriggers + LeptonTauTriggers + SingleLeptonTriggers + METTriggers

SetupSUSYTriggerTool().Triggers = TauTriggers
SetupSUSYTriggerTool().DisableSkimming = False
#SetupSUSYTriggerTool().OutputLevel = DEBUG
#ServiceMgr.MessageSvc.debugLimit = 2000000

STFile = "XAMPPstau/SUSYTools_Stau.conf"

SetupStauAnaHelper().fillLHEWeights = False
SetupStauAnaHelper().DoTauBJetOR = True

#SetupStauAnaHelper().createCommonTree = True
SetupSystematicsTool().pruneSystematics = [
    "EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1down",
    "EL_EFF_ChargeIDSel_TOTAL_1NPCOR_PLUS_UNCOR__1up",
    "MUON_EFF_BADMUON_STAT__1down",
    "MUON_EFF_BADMUON_STAT__1up",
    "MUON_EFF_BADMUON_SYS__1down",
    "MUON_EFF_BADMUON_SYS__1up",
    "MUON_EFF_RECO_STAT_LOWPT__1down",
    "MUON_EFF_RECO_STAT_LOWPT__1up",
    "MUON_EFF_RECO_SYS_LOWPT__1down",
    "MUON_EFF_RECO_SYS_LOWPT__1up",
    "EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down",
    "EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up",
    "EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down",
    "EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up",
]

### Get rid of the stupdily large event weights in the case we're running over sherpa
for sample, DSIDS in getLogicalDataSets().iteritems():
    if sample.find("Sherpa") != -1 and (getMCChannelNumber() in DSIDS or getRunNumbersMC() in DSIDS):
        print "We're running over a Sherpa sample (DSID: %d). Limit the GenWeight to be within 100." % (
            getRunNumbersMC() if getMCChannelNumber() == 0 else getMCChannelNumber())
        ### remove the crazy events from the meta data
        SetupStauAnaHelper().OutlierWeightStrategy = 1
        break

#####################################################################
#       Ensure that the matching branches are written to the tree   #
#####################################################################
SetupSUSYTriggerTool().StoreMatchingInfo = True
#use baseline objects for matching
SetupSUSYTriggerTool().SignalElecForMatching = False
SetupSUSYTriggerTool().SignalMuonForMatching = False
SetupSUSYTriggerTool().SignalTauForMatching = False

SetupSUSYTauSelector().TrigMatchingForTrigSF = True
SetupSUSYTauSelector().SFTrigger = SingleTauTriggers

ParseCommonStauOptions(STFile,
                       LepHadStream=False,
                       doEventShapes=False,
                       writeBaselineSF=False,
                       doMetSignificance=False,
                       includeTausInMet=True,
                       doTauPromotion=False,
                       useMC16d=True,
                       use17Data=True,
                       use18Data=True)
### List of DSIDs where Tau promotion shall be applied only
SetupStauTauSelector().doPromotionOnlyFor = []
SetupStauTauSelector().doFakeEff = False
SetupAlgorithm()
