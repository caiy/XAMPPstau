#! /usr/bin/env python
import os
import argparse
import commands
import sys
from ClusterSubmission.Utils import ReadListFromFile, ResolvePath
from ClusterSubmission.ListDisk import CheckRemainingProxyTime
from ClusterSubmission.PeriodRunConverter import GetPeriodRunConverter

rangeID = lambda start, end: range(start, end + 1)


def getLogicalDataSets():
    return {

        ### Z Sherpa
        "Sherpa221_Zmumu": [364359, 364362, 364281] + rangeID(364198, 364203) + rangeID(364100, 364113),
        "Sherpa221_Zee": [364358, 364361, 364280] + rangeID(364204, 364209) + rangeID(364114, 364127),
        "Sherpa221_Ztautau": [364360, 364363, 364282] + rangeID(364210, 364215) + rangeID(364128, 364141),
        ### Z PowHegPy8
        "PowHegPy8_DYee":
        rangeID(301000, 301018),
        "PowHegPy8_DYmumu":
        rangeID(301020, 301038),
        "PowHegPy8_DYtautau":
        rangeID(301040, 301058),
        "PowHegPy8_DYenu":
        rangeID(301060, 301098),
        "PowHegPy8_DYmunu":
        rangeID(301100, 301138),
        "PowHegPy8_DYtaunu":
        rangeID(301140, 301178),
        "PowHegPy8_Zmumu": [361666, 361667, 361107],
        "PowHegPy8_Zee": [361664, 361665, 361106],
        "PowHegPy8_Ztautau": [361668, 361669, 361108],
        ### Z MG5Py8: ee --> HT, mumu --> HT, tautau --> Np
        "MG5Py8_Zmumu":
        rangeID(363123, 363146),
        "MG5Py8_Zee":
        rangeID(363147, 363170),
        "MG5Py8_Ztautau":
        rangeID(361510, 361514),
        ### W+jets Sherpa
        "Sherpa221_Wmunu":
        rangeID(364156, 364169),
        "Sherpa221_Wenu":
        rangeID(364170, 364183),
        "Sherpa221_Wtaunu":
        rangeID(364184, 364197),
        ### W+jets Powheg
        "PowHegPy8_Wmunu": [361101, 361104],
        "PowHegPy8_Wenu": [361100, 361103],
        "PowHegPy8_Wtaunu": [361102, 361105],
        ### W+jets MG
        "MGPy8EG_Wmunu":
        rangeID(363624, 363647),
        "MGPy8EG_Wenu":
        rangeID(363600, 363623),
        "MGPy8EG_Wtaunu":
        rangeID(363648, 363671),
        ### ttbar
        "PowHegPy8_ttbar_incl":
        rangeID(410470, 410471),
        "PowHegPy8_ttbar_MET_sliced": [345935, 407345, 407346, 407347],
        "PowHegPy8_ttbar_HT_sliced": [407342, 407343, 407344],
        "aMcAtNloPy8EG_ttbar_incl": [410464, 410466],
        ### single-t (not sliced samples)
        "PowHegPy8_single_top_s_chan": [410644, 410645],
        "PowHegPy8_single_top_t_chan": [410658, 410659],
        "PowHegPy8_single_top_Wt_chan_incl": [410646, 410647],
        "PowHegPy8_single_top_Wt_chan_HT_sliced":
        rangeID(411181, 411186),
        "PowHegPy8_single_top_Wt_chan_MET_sliced":
        rangeID(411193, 411198),
        ### multi-t (3t, 4t)
        "MG5Py8_multi_t": [304014, 410080],
        ### ttV / tV / tVV
        "MG5_aMCatNLO_Py8_ttV_tV_tVV":
        [410218, 410276, 410397, 410219, 410277, 410398, 410220, 410278, 410399, 410156, 410157, 410155, 412063, 410408],
        ### Multibosons VV and VVV
        "Sherpa222_VV":
        rangeID(364288, 364290) + [364250] + rangeID(364253, 364255) + rangeID(363355, 363360) + [363489] + rangeID(364283, 364287) +
        [345705, 345706],
        "Sherpa221_VVV":
        rangeID(407311, 407315),
        ### Higgs: ggH (+WW + ZZ), VBF (+WW +ZZ), VH, ttH
        "Higgs":
        rangeID(345121, 345123) + [345324, 341471] + rangeID(346191, 346193) + [345948, 341488] + [345211, 345212, 345217] +
        rangeID(345873, 345875),
        ### Wh signal point
        "Wh_tau_190_60": [394480],
        "Wh_tau_200_0": [394481],
        "Wh_tau_375_0": [394534],
        ### Signal points
        "StauStau_80_1": [396114],
        "StauStau_100_1": [397049],
        "StauStau_100_60": [398348],
        "StauStau_120_1": [396102],
        "StauStau_120_40": [396107],
        "StauStau_120_80": [398349],
        "StauStau_140_1": [397048],
        "StauStau_140_40": [397047],
        "StauStau_160_1": [396103],
        "StauStau_160_100": [398350],
        "StauStau_160_40": [396108],
        "StauStau_160_80": [396112],
        "StauStau_180_1": [397046],
        "StauStau_180_40": [397045],
        "StauStau_200_1": [396104],
        "StauStau_200_120": [397011],
        "StauStau_200_1_mmix_s12": [396100],
        "StauStau_200_1_mmix_s12_Dm100": [396101],
        "StauStau_200_40": [396109],
        "StauStau_200_80": [397010],
        "StauStau_220_1": [397044],
        "StauStau_220_40": [397043],
        "StauStau_240_1": [396105],
        "StauStau_240_120": [397012],
        "StauStau_240_160": [397013],
        "StauStau_240_40": [396110],
        "StauStau_240_80": [396113],
        "StauStau_260_1": [397042],
        "StauStau_260_40": [397041],
        "StauStau_280_1": [396106],
        "StauStau_280_120": [397015],
        "StauStau_280_160": [397016],
        "StauStau_280_200": [398351],
        "StauStau_280_40": [396111],
        "StauStau_280_80": [397014],
        "StauStau_300_1": [397040],
        "StauStau_300_40": [397039],
        "StauStau_320_1": [397017],
        "StauStau_320_120": [397020],
        "StauStau_320_160": [397021],
        "StauStau_320_200": [397038],
        "StauStau_320_40": [397018],
        "StauStau_320_80": [397019],
        "StauStau_340_1": [397037],
        "StauStau_340_40": [397036],
        "StauStau_360_1": [397022],
        "StauStau_360_120": [397025],
        "StauStau_360_160": [397034],
        "StauStau_360_200": [397033],
        "StauStau_360_40": [397023],
        "StauStau_360_80": [397024],
        "StauStau_380_1": [397035],
        "StauStau_400_1": [397026],
        "StauStau_400_120": [397031],
        "StauStau_400_160": [397028],
        "StauStau_400_40": [397032],
        "StauStau_400_80": [397027],
        "StauStau_440_1": [397030],
        "StauStau_440_120": [398353],
        "StauStau_440_40": [397029],
        "StauStau_440_80": [398352],
        ### Gaugino Stau signal points
        "C1C1_Stau_100p0_0p0": [396293],
        "C1C1_Stau_100p0_35p0": [396294],
        "C1C1_Stau_100p0_65p0": [396295],
        "C1C1_Stau_112p5_12p5": [396296],
        "C1C1_Stau_112p5_47p5": [396297],
        "C1C1_Stau_117p5_82p5": [396298],
        "C1C1_Stau_130p0_30p0": [396299],
        "C1C1_Stau_132p5_67p5": [396300],
        "C1C1_Stau_142p5_107p5": [396301],
        "C1C1_Stau_150p0_50p0": [396302],
        "C1C1_Stau_157p5_92p5": [396303],
        "C1C1_Stau_167p5_132p5": [396304],
        "C1C1_Stau_175p0_25p0": [396305],
        "C1C1_Stau_175p0_75p0": [396306],
        "C1C1_Stau_182p5_117p5": [396307],
        "C1C1_Stau_192p5_157p5": [396308],
        "C1C1_Stau_200p0_0p0": [396309],
        "C1C1_Stau_200p0_100p0": [396310],
        "C1C1_Stau_200p0_50p0": [396311],
        "C1C1_Stau_207p5_142p5": [396312],
        "C1C1_Stau_217p5_182p5": [396313],
        "C1C1_Stau_225p0_125p0": [396314],
        "C1C1_Stau_225p0_75p0": [396315],
        "C1C1_Stau_232p5_167p5": [396316],
        "C1C1_Stau_242p5_207p5": [396317],
        "C1C1_Stau_250p0_100p0": [396318],
        "C1C1_Stau_250p0_150p0": [396319],
        "C1C1_Stau_257p5_192p5": [396320],
        "C1C1_Stau_267p5_232p5": [396321],
        "C1C1_Stau_275p0_125p0": [396322],
        "C1C1_Stau_275p0_175p0": [396323],
        "C1C1_Stau_282p5_217p5": [396324],
        "C1C1_Stau_300p0_0p0": [396325],
        "C1C1_Stau_300p0_100p0": [396326],
        "C1C1_Stau_300p0_150p0": [396327],
        "C1C1_Stau_300p0_200p0": [396328],
        "C1C1_Stau_325p0_125p0": [396329],
        "C1C1_Stau_325p0_175p0": [396330],
        "C1C1_Stau_325p0_225p0": [396331],
        "C1C1_Stau_330p0_295p0": [396332],
        "C1C1_Stau_345p0_280p0": [396333],
        "C1C1_Stau_350p0_150p0": [396334],
        "C1C1_Stau_350p0_200p0": [396335],
        "C1C1_Stau_362p5_262p5": [396336],
        "C1C1_Stau_375p0_175p0": [396337],
        "C1C1_Stau_387p5_237p5": [396338],
        "C1C1_Stau_400p0_0p0": [396339],
        "C1C1_Stau_400p0_100p0": [396340],
        "C1C1_Stau_400p0_300p0": [396341],
        "C1C1_Stau_412p5_212p5": [396342],
        "C1C1_Stau_425p0_275p0": [396343],
        "C1C1_Stau_450p0_250p0": [396344],
        "C1C1_Stau_450p0_350p0": [396345],
        "C1C1_Stau_475p0_325p0": [396346],
        "C1C1_Stau_500p0_0p0": [396347],
        "C1C1_Stau_500p0_100p0": [396348],
        "C1C1_Stau_500p0_200p0": [396349],
        "C1C1_Stau_500p0_300p0": [396350],
        "C1C1_Stau_500p0_400p0": [396351],
        "C1C1_Stau_525p0_375p0": [396352],
        "C1C1_Stau_550p0_350p0": [396353],
        "C1C1_Stau_550p0_450p0": [396354],
        "C1C1_Stau_575p0_425p0": [396355],
        "C1C1_Stau_600p0_0p0": [396356],
        "C1C1_Stau_600p0_100p0": [396357],
        "C1C1_Stau_600p0_200p0": [396358],
        "C1C1_Stau_600p0_300p0": [396359],
        "C1C1_Stau_600p0_400p0": [396360],
        "C1C1_Stau_600p0_500p0": [396361],
        "C1C1_Stau_625p0_475p0": [396362],
        "C1C1_Stau_650p0_350p0": [396363],
        "C1C1_Stau_650p0_450p0": [396364],
        "C1C1_Stau_650p0_550p0": [396365],
        "C1C1_Stau_675p0_525p0": [396366],
        "C1C1_Stau_700p0_0p0": [396367],
        "C1C1_Stau_700p0_100p0": [396368],
        "C1C1_Stau_700p0_200p0": [396369],
        "C1C1_Stau_700p0_300p0": [396370],
        "C1C1_Stau_700p0_400p0": [396371],
        "C1C1_Stau_700p0_500p0": [396372],
        "C1C1_Stau_700p0_600p0": [396373],
        "C1C1_Stau_725p0_575p0": [396374],
        "C1C1_Stau_750p0_450p0": [396375],
        "C1C1_Stau_750p0_550p0": [396376],
        "C1C1_Stau_800p0_0p0": [396377],
        "C1C1_Stau_800p0_100p0": [396378],
        "C1C1_Stau_800p0_200p0": [396379],
        "C1C1_Stau_800p0_300p0": [396380],
        "C1C1_Stau_800p0_400p0": [396381],
        "C1C1_Stau_800p0_500p0": [396382],
        "C1C1_Stau_800p0_600p0": [396383],
        "C1C1_Stau_900p0_0p0": [396384],
        "C1C1_Stau_900p0_100p0": [396385],
        "C1C1_Stau_900p0_200p0": [396386],
        "C1C1_Stau_900p0_300p0": [396387],
        "C1C1_Stau_900p0_400p0": [396388],
        "C1C1_Stau_900p0_500p0": [396389],
        "C1C1_Stau_900p0_600p0": [396390],
        "C1C1_Stau_1000p0_0p0": [396391],
        "C1C1_Stau_1000p0_100p0": [396392],
        "C1C1_Stau_1000p0_200p0": [396393],
        "C1C1_Stau_1000p0_300p0": [396394],
        "C1C1_Stau_1000p0_400p0": [396395],
        "C1C1_Stau_1000p0_500p0": [396396],
        "C1C1_Stau_1000p0_600p0": [396397],
        "C1C1_Stau_1100p0_0p0": [396398],
        "C1C1_Stau_1100p0_100p0": [396399],
        "C1C1_Stau_1100p0_200p0": [396400],
        "C1C1_Stau_1100p0_300p0": [396401],
        "C1C1_Stau_1100p0_400p0": [396402],
        "C1C1_Stau_1100p0_500p0": [396403],
        "C1C1_Stau_1100p0_600p0": [396404],
        "C1C1_Stau_1200p0_0p0": [396405],
        "C1C1_Stau_1200p0_100p0": [396406],
        "C1C1_Stau_1200p0_200p0": [396407],
        "C1C1_Stau_1200p0_300p0": [396408],
        "C1C1_Stau_1200p0_400p0": [396409],
        "C1C1_Stau_1200p0_500p0": [396410],
        "C1C1_Stau_1200p0_600p0": [396411],
        "C1N2_Stau_100p0_0p0": [396412],
        "C1N2_Stau_100p0_35p0": [396413],
        "C1N2_Stau_100p0_65p0": [396414],
        "C1N2_Stau_112p5_12p5": [396415],
        "C1N2_Stau_112p5_47p5": [396416],
        "C1N2_Stau_117p5_82p5": [396417],
        "C1N2_Stau_130p0_30p0": [396418],
        "C1N2_Stau_132p5_67p5": [396419],
        "C1N2_Stau_142p5_107p5": [396420],
        "C1N2_Stau_150p0_50p0": [396421],
        "C1N2_Stau_157p5_92p5": [396422],
        "C1N2_Stau_167p5_132p5": [396423],
        "C1N2_Stau_175p0_25p0": [396424],
        "C1N2_Stau_175p0_75p0": [396425],
        "C1N2_Stau_182p5_117p5": [396426],
        "C1N2_Stau_192p5_157p5": [396427],
        "C1N2_Stau_200p0_0p0": [396428],
        "C1N2_Stau_200p0_100p0": [396429],
        "C1N2_Stau_200p0_50p0": [396430],
        "C1N2_Stau_207p5_142p5": [396431],
        "C1N2_Stau_217p5_182p5": [396432],
        "C1N2_Stau_225p0_125p0": [396433],
        "C1N2_Stau_225p0_75p0": [396434],
        "C1N2_Stau_232p5_167p5": [396435],
        "C1N2_Stau_242p5_207p5": [396436],
        "C1N2_Stau_250p0_100p0": [396437],
        "C1N2_Stau_250p0_150p0": [396438],
        "C1N2_Stau_257p5_192p5": [396439],
        "C1N2_Stau_267p5_232p5": [396440],
        "C1N2_Stau_275p0_125p0": [396441],
        "C1N2_Stau_275p0_175p0": [396442],
        "C1N2_Stau_282p5_217p5": [396443],
        "C1N2_Stau_300p0_0p0": [396444],
        "C1N2_Stau_300p0_100p0": [396445],
        "C1N2_Stau_300p0_150p0": [396446],
        "C1N2_Stau_300p0_200p0": [396447],
        "C1N2_Stau_325p0_125p0": [396448],
        "C1N2_Stau_325p0_175p0": [396449],
        "C1N2_Stau_325p0_225p0": [396450],
        "C1N2_Stau_330p0_295p0": [396451],
        "C1N2_Stau_345p0_280p0": [396452],
        "C1N2_Stau_350p0_150p0": [396453],
        "C1N2_Stau_350p0_200p0": [396454],
        "C1N2_Stau_362p5_262p5": [396455],
        "C1N2_Stau_375p0_175p0": [396456],
        "C1N2_Stau_387p5_237p5": [396457],
        "C1N2_Stau_400p0_0p0": [396458],
        "C1N2_Stau_400p0_100p0": [396459],
        "C1N2_Stau_400p0_300p0": [396460],
        "C1N2_Stau_412p5_212p5": [396461],
        "C1N2_Stau_425p0_275p0": [396462],
        "C1N2_Stau_450p0_250p0": [396463],
        "C1N2_Stau_450p0_350p0": [396464],
        "C1N2_Stau_475p0_325p0": [396465],
        "C1N2_Stau_500p0_0p0": [396466],
        "C1N2_Stau_500p0_100p0": [396467],
        "C1N2_Stau_500p0_200p0": [396468],
        "C1N2_Stau_500p0_300p0": [396469],
        "C1N2_Stau_500p0_400p0": [396470],
        "C1N2_Stau_525p0_375p0": [396471],
        "C1N2_Stau_550p0_350p0": [396472],
        "C1N2_Stau_550p0_450p0": [396473],
        "C1N2_Stau_575p0_425p0": [396474],
        "C1N2_Stau_600p0_0p0": [396475],
        "C1N2_Stau_600p0_100p0": [396476],
        "C1N2_Stau_600p0_200p0": [396477],
        "C1N2_Stau_600p0_300p0": [396478],
        "C1N2_Stau_600p0_400p0": [396479],
        "C1N2_Stau_600p0_500p0": [396480],
        "C1N2_Stau_625p0_475p0": [396481],
        "C1N2_Stau_650p0_350p0": [396482],
        "C1N2_Stau_650p0_450p0": [396483],
        "C1N2_Stau_650p0_550p0": [396484],
        "C1N2_Stau_675p0_525p0": [396485],
        "C1N2_Stau_700p0_0p0": [396486],
        "C1N2_Stau_700p0_100p0": [396487],
        "C1N2_Stau_700p0_200p0": [396488],
        "C1N2_Stau_700p0_300p0": [396489],
        "C1N2_Stau_700p0_400p0": [396490],
        "C1N2_Stau_700p0_500p0": [396491],
        "C1N2_Stau_700p0_600p0": [396492],
        "C1N2_Stau_725p0_575p0": [396493],
        "C1N2_Stau_750p0_450p0": [396494],
        "C1N2_Stau_750p0_550p0": [396495],
        "C1N2_Stau_800p0_0p0": [396496],
        "C1N2_Stau_800p0_100p0": [396497],
        "C1N2_Stau_800p0_200p0": [396498],
        "C1N2_Stau_800p0_300p0": [396499],
        "C1N2_Stau_800p0_400p0": [396500],
        "C1N2_Stau_800p0_500p0": [396501],
        "C1N2_Stau_800p0_600p0": [396502],
        "C1N2_Stau_900p0_0p0": [396503],
        "C1N2_Stau_900p0_100p0": [396504],
        "C1N2_Stau_900p0_200p0": [396505],
        "C1N2_Stau_900p0_300p0": [396506],
        "C1N2_Stau_900p0_400p0": [396507],
        "C1N2_Stau_900p0_500p0": [396508],
        "C1N2_Stau_900p0_600p0": [396509],
        "C1N2_Stau_1000p0_0p0": [396510],
        "C1N2_Stau_1000p0_100p0": [396511],
        "C1N2_Stau_1000p0_200p0": [396512],
        "C1N2_Stau_1000p0_300p0": [396513],
        "C1N2_Stau_1000p0_400p0": [396514],
        "C1N2_Stau_1000p0_500p0": [396515],
        "C1N2_Stau_1000p0_600p0": [396516],
        "C1N2_Stau_1100p0_0p0": [396517],
        "C1N2_Stau_1100p0_100p0": [396518],
        "C1N2_Stau_1100p0_200p0": [396519],
        "C1N2_Stau_1100p0_300p0": [396520],
        "C1N2_Stau_1100p0_400p0": [396521],
        "C1N2_Stau_1100p0_500p0": [396522],
        "C1N2_Stau_1100p0_600p0": [396523],
        "C1N2_Stau_1200p0_0p0": [396524],
        "C1N2_Stau_1200p0_100p0": [396525],
        "C1N2_Stau_1200p0_200p0": [396526],
        "C1N2_Stau_1200p0_300p0": [396527],
        "C1N2_Stau_1200p0_400p0": [396528],
        "C1N2_Stau_1200p0_500p0": [396529],
        "C1N2_Stau_1200p0_600p0": [396530],
        ### Whtau
        "Wh_tau_190_60": [394480],
        "Wh_tau_200_0": [394481],
        "Wh_tau_375_0": [394534],
        "Wh_hall_150p0_0p0_1T": [396587],
        "Wh_hall_152p5_22p5_1T": [396588],
        "Wh_hall_162p5_12p5_1T": [396589],
        "Wh_hall_165p0_35p0_1T": [396590],
        "Wh_hall_175p0_0p0_1T": [396591],
        "Wh_hall_175p0_25p0_1T": [396592],
        "Wh_hall_177p5_47p5_1T": [396593],
        "Wh_hall_187p5_12p5_1T": [396594],
        "Wh_hall_187p5_37p5_1T": [396595],
        "Wh_hall_190p0_60p0_1T": [396596],
        "Wh_hall_200p0_0p0_1T": [396597],
        "Wh_hall_200p0_25p0_1T": [396598],
        "Wh_hall_200p0_50p0_1T": [396599],
        "Wh_hall_202p5_72p5_1T": [396600],
        "Wh_hall_212p5_37p5_1T": [396601],
        "Wh_hall_212p5_62p5_1T": [396602],
        "Wh_hall_225p0_0p0_1T": [396603],
        "Wh_hall_225p0_25p0_1T": [396604],
        "Wh_hall_225p0_50p0_1T": [396605],
        "Wh_hall_225p0_75p0_1T": [396606],
        "Wh_hall_237p5_62p5_1T": [396607],
        "Wh_hall_250p0_0p0_1T": [396608],
        "Wh_hall_250p0_100p0_1T": [396609],
        "Wh_hall_250p0_25p0_1T": [396610],
        "Wh_hall_250p0_50p0_1T": [396611],
        "Wh_hall_250p0_75p0_1T": [396612],
        "Wh_hall_275p0_0p0_1T": [396613],
        "Wh_hall_275p0_25p0_1T": [396614],
        "Wh_hall_275p0_50p0_1T": [396615],
        "Wh_hall_275p0_75p0_1T": [396616],
        "Wh_hall_300p0_0p0_1T": [396617],
        "Wh_hall_300p0_100p0_1T": [396618],
        "Wh_hall_300p0_25p0_1T": [396619],
        "Wh_hall_300p0_50p0_1T": [396620],
        "Wh_hall_300p0_75p0_1T": [396621],
        "Wh_hall_325p0_0p0_1T": [396622],
        "Wh_hall_325p0_50p0_1T": [396623],
        "Wh_hall_350p0_0p0_1T": [396624],
        "Wh_hall_350p0_100p0_1T": [396625],
        "Wh_hall_350p0_25p0_1T": [396626],
        "Wh_hall_350p0_50p0_1T": [396627],
        "Wh_hall_350p0_75p0_1T": [396628],
        "Wh_hall_375p0_0p0_1T": [396629],
        "Wh_hall_375p0_50p0_1T": [396630],
        "Wh_hall_400p0_0p0_1T": [396631],
        "Wh_hall_400p0_25p0_1T": [396632],
        "Wh_hall_425p0_0p0_1T": [396633],
        ### Stop-stau singal points
        "TT_stau_500_90": [437348],
        "TT_stau_500_490": [437349],
        "TT_stau_550_90": [437350],
        "TT_stau_550_190": [437351],
        "TT_stau_550_290": [437352],
        "TT_stau_550_390": [437353],
        "TT_stau_550_490": [437354],
        "TT_stau_550_540": [437355],
        "TT_stau_600_90": [437356],
        "TT_stau_600_540": [437357],
        "TT_stau_600_590": [437358],
        "TT_stau_650_90": [437359],
        "TT_stau_650_190": [437360],
        "TT_stau_650_290": [437361],
        "TT_stau_650_390": [437362],
        "TT_stau_650_490": [437363],
        "TT_stau_650_590": [437364],
        "TT_stau_650_640": [437365],
        "TT_stau_700_90": [437366],
        "TT_stau_700_640": [437367],
        "TT_stau_700_690": [437368],
        "TT_stau_750_90": [437369],
        "TT_stau_750_190": [437370],
        "TT_stau_750_290": [437371],
        "TT_stau_750_390": [437372],
        "TT_stau_750_490": [437373],
        "TT_stau_750_590": [437374],
        "TT_stau_750_690": [437375],
        "TT_stau_750_740": [437376],
        "TT_stau_800_90": [437377],
        "TT_stau_800_740": [437378],
        "TT_stau_800_790": [437379],
        "TT_stau_850_90": [437380],
        "TT_stau_850_190": [437381],
        "TT_stau_850_290": [437382],
        "TT_stau_850_390": [437383],
        "TT_stau_850_490": [437384],
        "TT_stau_850_590": [437385],
        "TT_stau_850_690": [437386],
        "TT_stau_850_790": [437387],
        "TT_stau_850_840": [437388],
        "TT_stau_900_90": [437389],
        "TT_stau_900_840": [437390],
        "TT_stau_900_890": [437391],
        "TT_stau_950_90": [437392],
        "TT_stau_950_190": [437393],
        "TT_stau_950_290": [437394],
        "TT_stau_950_390": [437395],
        "TT_stau_950_490": [437396],
        "TT_stau_950_590": [437397],
        "TT_stau_950_690": [437398],
        "TT_stau_950_790": [437399],
        "TT_stau_950_890": [437400],
        "TT_stau_950_940": [437401],
        "TT_stau_1000_90": [437402],
        "TT_stau_1000_890": [437403],
        "TT_stau_1000_990": [437404],
        "TT_stau_1050_90": [437405],
        "TT_stau_1050_190": [437406],
        "TT_stau_1050_290": [437407],
        "TT_stau_1050_390": [437408],
        "TT_stau_1050_490": [437409],
        "TT_stau_1050_590": [437410],
        "TT_stau_1050_690": [437411],
        "TT_stau_1050_790": [437412],
        "TT_stau_1050_890": [437413],
        "TT_stau_1050_990": [437414],
        "TT_stau_1050_1040": [437415],
        "TT_stau_1100_90": [437416],
        "TT_stau_1100_190": [437417],
        "TT_stau_1100_890": [437418],
        "TT_stau_1100_990": [437419],
        "TT_stau_1100_1090": [437420],
        "TT_stau_1150_90": [437421],
        "TT_stau_1150_190": [437422],
        "TT_stau_1150_290": [437423],
        "TT_stau_1150_390": [437424],
        "TT_stau_1150_490": [437425],
        "TT_stau_1150_590": [437426],
        "TT_stau_1150_690": [437427],
        "TT_stau_1150_790": [437428],
        "TT_stau_1150_890": [437429],
        "TT_stau_1150_990": [437430],
        "TT_stau_1150_1090": [437431],
        "TT_stau_1150_1140": [437432],
        "TT_stau_1200_90": [437433],
        "TT_stau_1200_190": [437434],
        "TT_stau_1200_290": [437435],
        "TT_stau_1200_390": [437436],
        "TT_stau_1200_490": [437437],
        "TT_stau_1200_990": [437438],
        "TT_stau_1200_1090": [437439],
        "TT_stau_1200_1190": [437440],
        "TT_stau_1250_90": [437441],
        "TT_stau_1250_190": [437442],
        "TT_stau_1250_290": [437443],
        "TT_stau_1250_390": [437444],
        "TT_stau_1250_490": [437445],
        "TT_stau_1250_590": [437446],
        "TT_stau_1250_690": [437447],
        "TT_stau_1250_790": [437448],
        "TT_stau_1250_890": [437449],
        "TT_stau_1250_990": [437450],
        "TT_stau_1250_1090": [437451],
        "TT_stau_1250_1190": [437452],
        "TT_stau_1250_1240": [437453],
        "TT_stau_1350_90": [437454],
        "TT_stau_1350_190": [437455],
        "TT_stau_1350_290": [437456],
        "TT_stau_1350_390": [437457],
        "TT_stau_1350_490": [437458],
        "TT_stau_1350_590": [437459],
        "TT_stau_1350_690": [437460],
        "TT_stau_1350_790": [437461],
        "TT_stau_1350_890": [437462],
        "TT_stau_1350_990": [437463],
        "TT_stau_1350_1090": [437464],
        "TT_stau_1350_1190": [437465],
        "TT_stau_1300_1290": [437530],
        "TT_stau_1350_1290": [437531],
        "TT_stau_1350_1340": [437532],
        "TT_stau_1400_1390": [437533],
        "TT_stau_1450_1090": [437534],
        "TT_stau_1450_1290": [437535],
        "TT_stau_1450_1390": [437536],
        "TT_stau_1450_1440": [437537],
        "TT_stau_1450_190": [437538],
        "TT_stau_1450_290": [437539],
        "TT_stau_1450_490": [437540],
        "TT_stau_1450_690": [437541],
        "TT_stau_1450_890": [437542],
        "TT_stau_1450_90": [437543],
        "TT_stau_1500_1490": [437544],
        "TT_stau_1550_1090": [437545],
        "TT_stau_1550_1290": [437546],
        "TT_stau_1550_1490": [437547],
        "TT_stau_1550_1540": [437548],
        "TT_stau_1550_190": [437549],
        "TT_stau_1550_290": [437550],
        "TT_stau_1550_490": [437551],
        "TT_stau_1550_690": [437552],
        "TT_stau_1550_890": [437553],
        "TT_stau_1550_90": [437554],
        "TT_stau_1600_1590": [437555],
        "TT_stau_1650_1090": [437556],
        "TT_stau_1650_1290": [437557],
        "TT_stau_1650_1490": [437558],
        "TT_stau_1650_1590": [437559],
        "TT_stau_1650_1640": [437560],
        "TT_stau_1650_190": [437561],
        "TT_stau_1650_290": [437562],
        "TT_stau_1650_490": [437563],
        "TT_stau_1650_690": [437564],
        "TT_stau_1650_890": [437565],
        "TT_stau_1650_90": [437566],
        "TT_stau_1700_1690": [437567],
        "TT_stau_1750_1090": [437568],
        "TT_stau_1750_1290": [437569],
        "TT_stau_1750_1490": [437570],
        "TT_stau_1750_1690": [437571],
        "TT_stau_1750_1740": [437572],
        "TT_stau_1750_190": [437573],
        "TT_stau_1750_290": [437574],
        "TT_stau_1750_490": [437575],
        "TT_stau_1750_690": [437576],
        "TT_stau_1750_890": [437577],
        "TT_stau_1750_90": [437578],
        "TT_stau_1800_1790": [437579],
        "TT_stau_1850_1090": [437580],
        "TT_stau_1850_1290": [437581],
        "TT_stau_1850_1490": [437582],
        "TT_stau_1850_1690": [437583],
        "TT_stau_1850_1790": [437584],
        "TT_stau_1850_1840": [437585],
        "TT_stau_1850_190": [437586],
        "TT_stau_1850_290": [437587],
        "TT_stau_1850_490": [437588],
        "TT_stau_1850_690": [437589],
        "TT_stau_1850_890": [437590],
        "TT_stau_1850_90": [437591],
        ### 3rd generation LQ signal points
        "LQu_M300": [310550],
        "LQd_M300": [310551],
        "LQu_M500": [310552],
        "LQd_M500": [310553],
        "LQu_M900": [310554],
        "LQd_M900": [310555],
        "LQu_M1300": [310556],
        "LQd_M1300": [310557],
        "LQu_M1700": [310558],
        "LQd_M1700": [310559],
        "LQu_M400": [312201],
        "LQd_M400": [312202],
        "LQu_M600": [312203],
        "LQd_M600": [312204],
        "LQu_M700": [312205],
        "LQd_M700": [312206],
        "LQu_M800": [312207],
        "LQd_M800": [312208],
        "LQu_M850": [312209],
        "LQd_M850": [312210],
        "LQu_M950": [312211],
        "LQd_M950": [312212],
        "LQu_M1000": [312213],
        "LQd_M1000": [312214],
        "LQu_M1050": [312215],
        "LQd_M1050": [312216],
        "LQu_M1100": [312217],
        "LQd_M1100": [312218],
        "LQu_M1150": [312219],
        "LQd_M1150": [312220],
        "LQu_M1200": [312221],
        "LQd_M1200": [312222],
        "LQu_M1250": [312223],
        "LQd_M1250": [312224],
        "LQu_M1350": [312225],
        "LQd_M1350": [312226],
        "LQu_M1400": [312227],
        "LQd_M1400": [312228],
        "LQu_M1450": [312229],
        "LQd_M1450": [312230],
        "LQu_M1500": [312231],
        "LQd_M1500": [312232],
        "LQu_M1550": [312233],
        "LQd_M1550": [312234],
        "LQu_M1600": [312235],
        "LQd_M1600": [312236],
        "LQu_M1800": [312237],
        "LQd_M1800": [312238],
        "LQu_M1900": [312239],
        "LQd_M1900": [312240],
        "LQu_M2000": [312241],
        "LQd_M2000": [312242],
    }


def GetDSID(DS):
    if DS.find(":") > -1:
        SubmittingAcc = DS[:DS.find(":")]
        DSName = DS[DS.find(SubmittingAcc, DS.find(":") + 1) + len(SubmittingAcc) + 1:]
    else:
        DSName = DS
    #Assume the structure: mc16_13TeV.410025.S18.LepHad.V04_XAMPP
    if not DSName.startswith("mc"): return -1
    dsid = int(DSName[DSName.find(".") + 1:DSName.find(".", DSName.find(".") + 1)])
    return dsid


def GetRunNumber(DS):
    if DS.find(":") > -1:
        SubmittingAcc = DS[:DS.find(":")]
        DSName = DS[DS.find(SubmittingAcc, DS.find(":") + 1) + len(SubmittingAcc) + 1:]
    else:
        DSName = DS

    #Assume the structure data16_13TeV.<DSID>.<Blah>
    if not DSName.startswith("data"): return -1
    try:
        dsid = int(DSName[DSName.find(".") + 1:DSName.find(".", DSName.find(".") + 1)])
    except:
        return -1
    return dsid


def GetYearAndPeriod(DSName):
    #Assume: data15_13TeV.periodH.physics_Main.PhysCont.XAMPPstau.v01.grp15_v01_p3213_XAMPP

    if not DSName.startswith("data"): return -1
    period = [elem for elem in DSName.split(".") if elem.startswith('period')][-1].strip('period')
    year = [elem for elem in DSName.split(".") if elem.startswith('data')][-1].replace('data', "").replace("_13TeV", "")
    if not period or not year:
        return "", ""
    return year, period


def GetLogicalDataSetName(DS):
    SubmittingAcc = DS[:DS.find(":")]
    DSName = DS if DS.find(":") == -1 else DS[DS.find(":") + 1:]
    ID = GetDSID(DSName)
    if ID != -1:
        for Logical, RunRanges in getLogicalDataSets().iteritems():
            if ID in RunRanges: return Logical
    else:
        RunNumber = GetRunNumber(DSName)
        year, period = GetYearAndPeriod(DSName)
        if year and period: return "data%s_period%s" % (year, period)
    return DSName


def getAnalyses():
    return {
        "LepHad": "XAMPPstau/runLepHad.py",
        "HadHad": "XAMPPstau/runHadHad.py",
        "DirectStauTruth": "XAMPPstau/runTruth.py",
        "Trigger": "XAMPPstau/runTriggerStudies.py",
    }


def setupGridSubmitArgParser():
    parser = argparse.ArgumentParser(
        description='This script submits the analysis code to the grid. For more help type \"python XAMPPstau/python/SubmitToGrid.py -h\"',
        prog='SubmitToGrid',
        conflict_handler='resolve',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--noSyst', help='No systematics will be submitted', action="store_true", default=False)
    parser.add_argument('--DuplicateTask', help='Allow for task duplcation', action="store_true", default=False)

    parser.add_argument('--stream',
                        help='What analysis stream should be assigned',
                        choices=[ana for ana in getAnalyses().iterkeys()],
                        required=True)
    parser.add_argument("--production", help="Name of the production", required=True)

    parser.add_argument("--sampleList", help="Name of the production", required=True, nargs='+')
    parser.add_argument('--productionRole', help='specify optionally a production role you have e.g. perf-muons', default='')
    parser.add_argument(
        "--physicalSamples",
        help="Only submit datasets which belong to this list of physical samples",
        nargs='+',
        default=[],
        #choices = [DS for DS in getLogicalDataSets().iterkeys()] + []
    )

    return parser


if __name__ == '__main__':
    RunOptions = setupGridSubmitArgParser().parse_args()

    while CheckRemainingProxyTime() < 100:
        continue
    ### Do not use the AODs since they are place holders in the list
    All_DS = []
    for Smp in RunOptions.sampleList:
        All_DS += [F for F in ReadListFromFile(Smp) if F.find("DAOD") != -1]

    ### Build the dictionary
    Samples_ToSubmit = {}

    for DS in All_DS:
        Logical_Name = GetLogicalDataSetName(DS)
        if len(RunOptions.physicalSamples) > 0 and Logical_Name not in RunOptions.physicalSamples:
            continue

        try:
            Samples_ToSubmit[Logical_Name] += [DS]
        except:
            Samples_ToSubmit[Logical_Name] = [DS]

    SubmitScript = ResolvePath("XAMPPbase/python/SubmitToGrid.py")
    for Sample, DataSetsToSubmit in Samples_ToSubmit.iteritems():
        n_files_per_job = 15
        if not "data" in Sample and RunOptions.noSyst: n_files_per_job = 2
        elif not RunOptions.noSyst: n_files_per_job = 1
        Cmd = "python %s  --jobOptions %s --inputDS %s --outDS XAMPP.%s.%s.%s %s %s %s --nFilesPerJob %d --noAmiCheck" % (
            SubmitScript, getAnalyses()[RunOptions.stream], ",".join(DataSetsToSubmit), RunOptions.stream, RunOptions.production, Sample,
            "--noSyst" if RunOptions.noSyst else "", "" if len(RunOptions.productionRole) == 0 else "--productionRole %s" %
            (RunOptions.productionRole), "--DuplicateTask" if RunOptions.DuplicateTask else "", n_files_per_job)
        os.system(Cmd)

    Skipped_DS = []
    for Smp in RunOptions.sampleList:
        Skipped_DS += [F for F in ReadListFromFile(Smp) if F.find("DAOD") == -1]
    if len(Skipped_DS) > 0:
        print "WARNING: The following datasets were not submitted since they are no DAODs"
        for S in Skipped_DS:
            print "    --- %s" % (S)
