#!/bin/bash

##############################
# Setup                      #
##############################
#prepare AthAnalysis or build if not already done so
if [ -f /xampp/build/${AthAnalysis_PLATFORM}/setup.sh ]; then
    if [[ -z "${TestArea}" ]]; then
        export TestArea=/xampp/XAMPPstau
    fi
    source /xampp/build/${AthAnalysis_PLATFORM}/setup.sh
else
    asetup AthAnalysis,latest,here
    if [ -f ${TestArea}/build/${AthAnalysis_PLATFORM}/setup.sh ]; then
        source ${TestArea}/build/${AthAnalysis_PLATFORM}/setup.sh
    else
        mkdir -p ${TestArea}/build && cd ${TestArea}/build
        cmake ..
        cmake --build .
        cd .. && source build/${AthAnalysis_PLATFORM}/setup.sh
    fi
fi



# definition of folder for storing test results
EOS_PATH="root://eoshome.cern.ch//eos/user/x/xampp/ci/stau/"
mkdir -p ${CI_PROJECT_DIR}
cd ${CI_PROJECT_DIR}

# get kerberos token
if [ -z ${SERVICE_PASS} ]; then
  echo "You did not set the environment variable SERVICE_PASS.\n\
Please define in the gitlab project settings/CI the secret variables SERVICE_PASS and CERN_USER."
  exit 1
else
  echo "${SERVICE_PASS}" | kinit ${CERN_USER}@CERN.CH
fi


echo "" > FileList.txt
while read -r to_process; do
  file_name=${to_process#*/}
  if [ ! -f "${file_name}" ]; then
     echo "File ${to_process} not found! Copying it from EOS"
     echo "xrdcp ${EOS_PATH}${to_process} ${file_name}"
     xrdcp "${EOS_PATH}${to_process}" ${file_name}
     if [ $? -ne 0 ];then
        echo "Download failed"
        exit 1
    fi
     echo "${PWD}/${file_name}" >> FileList.txt
  fi
done < "/xampp/XAMPPstau/XAMPPstau/test/datasamples/${jobName}.txt"
echo "#########################################################################"
ls -lh
echo "#########################################################################"

# run job
echo "python ${TestArea}/XAMPPstau/python/runAthena.py --parseFilesForPRW --inFile FileList.txt --outFile ${jobName}.root --jobOptions ${JOBOPTIONS} --noSyst  ${EXTRARGS}"
python ${TestArea}/XAMPPstau/python/runAthena.py --inFile FileList.txt --parseFilesForPRW --outFile ${jobName}.root --jobOptions ${JOBOPTIONS} --noSyst  ${EXTRARGS}
###################################################
# Raise error if execution failed                 #
###################################################
if [ $? -ne 0 ]; then
  printf '%s\n' "Execution of runAthena.py failed" >&2  # write error message to stderr
  exit 1
fi


##############################
# Evalulate cut flows        #
##############################
if [ -z ${CutFlows} ];then
   echo "No CutFlows variable is set"
   exit 1
fi

for R in ${CutFlows}; do
    echo "Check raw cutflow of ${R}"
    python ${TestArea}/XAMPPbase/XAMPPbase/python/printCutFlow.py -i ${jobName}.root -a ${R} | tee ${CI_PROJECT_DIR}/${STREAM}_${jobName}_${R}.txt
    if [ $? -ne 0 ];then
        echo "Failed to retrieve raw cutflow for ${R}"
        exit 1
    fi    
    echo "Check weighted cutflow of ${R}"
    python ${TestArea}/XAMPPbase/XAMPPbase/python/printCutFlow.py -i ${jobName}.root -a ${R} --weighted | tee ${CI_PROJECT_DIR}/${STREAM}_${jobName}_${R}_weighted.txt
    if [ $? -ne 0 ];then
        echo "Failed to retrieve weighted cutflow for ${R}"
        exit 1
    fi
done
