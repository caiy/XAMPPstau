#ifndef XAMPPStau_StauHadHadAnalysisConfig_h
#define XAMPPStau_StauHadHadAnalysisConfig_h
#include <XAMPPbase/AnalysisConfig.h>

namespace XAMPP {
    class StauHadHadAnalysisConfig : public AnalysisConfig {
    public:
        ASG_TOOL_CLASS(StauHadHadAnalysisConfig, XAMPP::IAnalysisConfig)
        StauHadHadAnalysisConfig(std::string Analysis = "Staus");
        virtual ~StauHadHadAnalysisConfig() {}

        /** @brief     When we use particle flow jets we need to reject the events where the
         *             electrons or the photons that are entering the MET calculation
         *             have the decoration DFCommonCrackVetoCleaning set to false.
         *             This flag is set to true when using PFLow jets to allow rejecting those events.
         */
        bool m_doPFlowCleaning;

    protected:
        virtual StatusCode initializeCustomCuts();
    };
}  // namespace XAMPP
#endif
