#ifndef StauMCTModule_H
#define StauMCTModule_H

#include <XAMPPstau/StauAnalysisModule.h>

namespace XAMPP {
    class StauMCTModule : public StauAnalysisModule {
    public:
        // Create a proper constructor for Athena
        ASG_TOOL_CLASS(StauMCTModule, XAMPP::IAnalysisModule)
        //
        StauMCTModule(const std::string& myname);
        virtual StatusCode bookVariables();
        virtual StatusCode fill();

        virtual ~StauMCTModule();

    private:
        XAMPP::Storage<float>* m_dec_MCT;
    };
}  // namespace XAMPP
#endif
