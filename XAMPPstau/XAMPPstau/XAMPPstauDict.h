#ifndef XAMPPSTAU_XAMPPSTAUDICT_H
#define XAMPPSTAU_XAMPPSTAUDICT_H

#include <XAMPPstau/EventShapeModule.h>
#include <XAMPPstau/StauAnalysisHelper.h>
#include <XAMPPstau/StauHadHadAnalysisConfig.h>
#include <XAMPPstau/StauJigSawSusyModule.h>
#include <XAMPPstau/StauJigSawWModule.h>
#include <XAMPPstau/StauJigSawZModule.h>
#include <XAMPPstau/StauLepHadAnalysisConfig.h>
#include <XAMPPstau/StauMCTModule.h>
#include <XAMPPstau/StauMT2Module.h>
#include <XAMPPstau/StauMeffModule.h>
#include <XAMPPstau/StauSampleOverlap.h>
#include <XAMPPstau/StauTauSelector.h>
#include <XAMPPstau/StauTriggerTool.h>
#include <XAMPPstau/StauTruthAnalysisConfig.h>
#include <XAMPPstau/StauTruthAnalysisHelper.h>
#endif
