#ifndef XAMPPStau_StauLepHadAnalysisConfig_h
#define XAMPPStau_StauLepHadAnalysisConfig_h
#include <XAMPPbase/AnalysisConfig.h>

namespace XAMPP {
    class StauLepHadAnalysisConfig : public AnalysisConfig {
    public:
        ASG_TOOL_CLASS(StauLepHadAnalysisConfig, XAMPP::IAnalysisConfig)
        StauLepHadAnalysisConfig(const std::string &tool_name);
        virtual ~StauLepHadAnalysisConfig() {}

    protected:
        virtual StatusCode initializeCustomCuts();
    };

}  // namespace XAMPP
#endif
