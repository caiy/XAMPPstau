#ifndef StauAnalysisHelper_H
#define StauAnalysisHelper_H
#include <AsgTools/AnaToolHandle.h>
#include <AsgTools/ToolHandle.h>
#include <IsolationSelection/IIsolationSelectionTool.h>
#include <XAMPPbase/SUSYAnalysisHelper.h>
#include <XAMPPstau/StauSampleOverlap.h>

#include <fstream>
#include <iostream>
#include <sstream>

namespace XAMPP {
    class StauAnalysisHelper : public SUSYAnalysisHelper {
    public:
        // Create a proper constructor for Athena
        ASG_TOOL_CLASS(StauAnalysisHelper, XAMPP::IAnalysisHelper)
        //
        StauAnalysisHelper(const std::string& myname);

        virtual StatusCode RemoveOverlap();

        virtual ~StauAnalysisHelper();

        virtual bool AcceptEvent();

    protected:
        virtual StatusCode initializeAnalysisTools();
        virtual StatusCode ComputeEventVariables();

        virtual StatusCode initializeEventVariables();
        virtual StatusCode initializeModuleContainers();

        virtual StatusCode initializeLightLeptonTree();
        virtual StatusCode initializeJetTree();
        virtual StatusCode initializeTauTree();
        virtual StatusCode initializeDiLeptonTree();

    private:
        bool m_mergeSamples;
        bool m_doTauPromotion;
        bool m_doTauBJetOR;
        bool m_doTrackMet;

        /** @brief     When we use particle flow jets we need to reject the events where the
         *             electrons or the photons that are entering the MET calculation
         *             have the decoration DFCommonCrackVetoCleaning set to false.
         *             This flag is set to true when using PFLow jets to allow rejecting those events.
         */
        bool m_doPFlowCleaning;

        asg::AnaToolHandle<ISampleOverlap> m_smp_OR_removal;
        ToolHandleArray<CP::IIsolationSelectionTool> m_isoWPs;

        struct IsolationTester {
            SelectionDecorator particleDecoration;
            std::string name;
            ToolHandle<CP::IIsolationSelectionTool> isoTool;
            IsolationTester(const std::string& n, const ToolHandle<CP::IIsolationSelectionTool>& tool) {
                particleDecoration = std::make_unique<CharDecorator>("isol_" + n);
                name = n;
                isoTool = tool;
            }
        };
        std::vector<IsolationTester> m_isoTester_Elecs;
        std::vector<IsolationTester> m_isoTester_Muons;
    };

}  // namespace XAMPP
#endif
