#ifndef StauMeffModule_H
#define StauMeffModule_H

#include <XAMPPstau/StauAnalysisModule.h>

namespace XAMPP {
    class StauMeffModule : public StauAnalysisModule {
    public:
        // Create a proper constructor for Athena
        ASG_TOOL_CLASS(StauMeffModule, XAMPP::IAnalysisModule)
        //
        StauMeffModule(const std::string& myname);
        virtual StatusCode bookVariables();
        virtual StatusCode fill();

        virtual ~StauMeffModule();

    private:
        XAMPP::ParticleStorage* m_Leptons;

        XAMPP::Storage<float>* m_dec_HT_jet;
        XAMPP::Storage<float>* m_dec_HT_lep;
        XAMPP::Storage<float>* m_dec_HT_tau;
        XAMPP::Storage<float>* m_dec_Meff;

        XAMPP::Storage<float>* m_dec_Meff_tau;

        XAMPP::Storage<float>* m_dec_Pt_Eff;
        XAMPP::Storage<float>* m_dec_Pt_Eff2;
        XAMPP::Storage<float>* m_dec_Vec_SumPt;
        XAMPP::Storage<float>* m_dec_Vec_SumPhi;

        XAMPP::Storage<float>* m_dec_Met_Centrality;
        XAMPP::Storage<bool>* m_dec_Met_BiSect;

        XAMPP::Storage<float>* m_dec_SumCosDeltaPhi;
        XAMPP::Storage<float>* m_dec_CosMinDeltaPhi;

        XAMPP::Storage<float>* m_dec_PTt;
        XAMPP::Storage<float>* m_dec_dPhiTauLepMET;

        XAMPP::Storage<float>* m_dec_CosChi1;
        XAMPP::Storage<float>* m_dec_CosChi2;
        /// m_{tautau} variable defined as
        /// vectorial sum of the two leptons builds the met
        /// times some scaling factor
        /// p_{T}^{miss} = x * P_{T}^{l1} + y * P_{T}^{l2}

        /// m_{tautau} = 2* p_{l1} dot p+{l2}*(1+x)(1+y)
        XAMPP::Storage<float>* m_dec_mTauTau2;
        // XAMPP::Storage<float>* m_dec_MCT;

        std::string m_lepton_container;
        bool m_save_had_had;
        bool m_save_meff;
        bool m_save_lep_met;
        bool m_save_plane_angles;
    };
}  // namespace XAMPP
#endif
